import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PagarPage } from './pagar/pagar.page';
import { ReceberPage } from './receber/receber.page';
import { CadastroPage } from './cadastro/cadastro.page';
import { RelatorioPage } from './relatorio/relatorio.page';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

const routes: Routes = [
    {
        path: '', children: [
            { path: 'pagar', component: PagarPage },
            { path: 'receber', component: ReceberPage },
            { path: 'cadastro', component: CadastroPage },
            { path: 'relatorio', component: RelatorioPage },
        ]
    }
];

@NgModule({
    imports: [
        CommonModule,
        IonicModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule.forChild(routes)],
    declarations: [
        PagarPage,
        ReceberPage,
        CadastroPage,
        RelatorioPage
    ]
})
export class ContasRoutingModule { }
