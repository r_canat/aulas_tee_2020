import { Injectable } from '@angular/core';
import { NavComponentWithProps, NavController, ToastController } from '@ionic/angular';
import { AngularFireAuth } from '@angular/fire/auth';
import { Observable } from 'rxjs';
import { User } from 'firebase';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
    isLoggedIn: Observable<User>;

    constructor(
        private nav: NavController,
        private toast: ToastController,
        private auth: AngularFireAuth,

    ) {
        this.isLoggedIn = auth.authState;
    }

    login(user) {
        this.auth.signInWithEmailAndPassword(user.email, user.password).
            then(() => this.nav.navigateForward('home')).
            catch(() => this.showError());
    }

    private async showError() {
        const ctrl = await this.toast.create({
            message: 'Dados de acesso incorretos',
            duration: 3000
        });

        ctrl.present();
    }

    recoverPass(data) {
        this.auth.sendPasswordResetEmail(data.email).then(() => this.nav.navigateBack('auth')).
            catch(err => {
                console.log(err);
            });
    }

    createUser(user) {
        this.auth.createUserWithEmailAndPassword(user.email, user.password).then(credentials => console.log(credentials));
    }

    logout() {
        this.auth.signOut().then(() => this.nav.navigateBack('auth'));
    }
}
