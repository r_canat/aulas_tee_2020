// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
    production: false,
    firebaseConfig: {
        apiKey: 'AIzaSyAuWYfVb3bYI114SGpSN01lCeRTDy-kjfI',
        authDomain: 'controle-ifsp-a3ea9.firebaseapp.com',
        databaseURL: 'https://controle-ifsp-a3ea9.firebaseio.com',
        projectId: 'controle-ifsp-a3ea9',
        storageBucket: 'controle-ifsp-a3ea9.appspot.com',
        messagingSenderId: '435349480087',
        appId: '1:435349480087:web:d69c5a7b44f51bd432bfd2',
        measurementId: 'G-QFF2K6CTS7'
    }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
